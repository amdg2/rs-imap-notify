/*
extern crate imap;
extern crate native_tls;
extern crate chrono;
extern crate mailparse;
#[macro_use]
extern crate serde;
extern crate toml;
*/

use std::collections::{hash_map::HashMap, HashSet};
use std::fs::{self, DirBuilder};
use std::path::{Path, PathBuf};

use chrono::prelude::*;
use chrono::{DateTime, FixedOffset};
use directories::ProjectDirs;
use futures::{future, Sink, Stream};
use imap;
use mailparse::{parse_headers, MailHeaderMap};
use native_tls;
use serde_derive::{Deserialize, Serialize};
use tokio::runtime::current_thread::Runtime;
use tokio_xmpp::{xmpp_codec::Packet, Client};
use toml;
use xmpp_parsers::{
    message::{Body, Message},
    Jid,
};

type ImapResult<T> = std::result::Result<T, imap::error::Error>;

type ImapClient = imap::Client<native_tls::TlsStream<std::net::TcpStream>>;

type ImapSession = imap::Session<native_tls::TlsStream<std::net::TcpStream>>;

type ImapVec<T> = imap::types::ZeroCopy<Vec<T>>;

fn get_imap_directories(s: &mut ImapSession) -> ImapResult<ImapVec<imap::types::Name>> {
    s.list(Some("*"), Some("*"))
}

fn list_imap_directories(s: &mut ImapSession) {
    let directories = get_imap_directories(s).expect("Cannot list IMAP directories");

    for d in &directories {
        println!("{}", d.name());
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct ImapConfig {
    domain: String,
    port: Option<u16>,
    username: String,
    password: String,
    folders: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct XmppConfig {
    jid: String,
    password: String,
    recipient_jid: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Config {
    imap: ImapConfig,
    xmpp: XmppConfig,
}

enum FilePath<'a> {
    DefaultUser,
    DefaultSystem,
    Custom(&'a str),
}

fn get_config(config_file_path: FilePath) -> Config {
    let config_file_name = "config.toml";
    let path = match config_file_path {
        FilePath::DefaultUser => get_project_dirs().config_dir().join(config_file_name),
        FilePath::DefaultSystem => Path::new("/etc/rs-imap-notify").join(config_file_name),
        FilePath::Custom(path_str) => PathBuf::from(path_str),
    };

    if path.exists() {
        let file_raw_content = fs::read(path).expect("Cannot read config file");
        let file_utf8_content = String::from_utf8_lossy(file_raw_content.as_slice());

        toml::from_str(file_utf8_content.as_ref()).expect("Unable to parse config file")
    } else {
        panic!("Config file doens't exists at {}", path.display());
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct LatestInformation {
    datetime_by_folder: HashMap<String, DateTime<FixedOffset>>,
}

fn get_project_dirs() -> ProjectDirs {
    ProjectDirs::from("fr.feildel.baudouin", "Jehba Apps", "rs-imap-notify")
        .expect("Fatal: no home directory available")
}

fn get_latest_file_path(file_path: FilePath) -> PathBuf {
    let latest_file_name = "latest.toml";
    match file_path {
        FilePath::DefaultUser => get_project_dirs().data_dir().join(latest_file_name),
        FilePath::DefaultSystem => Path::new("/var/lib/rs-imap-notify").join(latest_file_name),
        FilePath::Custom(path_str) => PathBuf::from(path_str),
    }
}

fn get_latest_information(latest_file_path: FilePath, folders: &[String]) -> LatestInformation {
    let path = get_latest_file_path(latest_file_path);
    if path.exists() {
        // Assume we can read the file if metadata call works
        let file_raw_content = fs::read(path).expect("Cannot read latest file");
        let file_utf8_content = String::from_utf8_lossy(file_raw_content.as_slice());

        toml::from_str(file_utf8_content.as_ref()).expect("Unable to parse latest file")
    } else {
        // Latest file doesn't exists generate default one content for it
        let local_today = Local::today().and_hms(0, 0, 0);
        let fixed_today =
            DateTime::<FixedOffset>::from_utc(local_today.naive_utc(), *local_today.offset());

        let mut datetime_by_folder = HashMap::new();

        for f in folders {
            datetime_by_folder.insert(f.clone(), fixed_today);
        }

        // Return LatestInformation with current date as latest for every folder
        LatestInformation { datetime_by_folder }
    }
}

fn save_latest_information(latest_file_path: FilePath, latest: LatestInformation) {
    let file_path = get_latest_file_path(latest_file_path);
    let directory = file_path
        .parent()
        .expect("Latest file path should have a parent");
    if !directory.exists() {
        DirBuilder::new()
            .recursive(true)
            .create(directory)
            .expect("Cannot create directory to store latest information file");
    }

    let serialized_latest =
        toml::to_string(&latest).expect("Unable to serialize LatestInformation");
    fs::write(file_path, serialized_latest).expect("Unable to write latest file");
}

fn get_imap_session(config: &ImapConfig) -> ImapSession {
    let tls = native_tls::TlsConnector::builder().build().unwrap();
    let client = imap::connect(
        (config.domain.as_str(), config.port.unwrap_or(993)),
        config.domain.to_owned(),
        &tls,
    )
    .unwrap();
    client
        .login(config.username.to_owned(), config.password.to_owned())
        .expect("Unable to login, probably bad credentials")
}

fn get_latest_unread_uid(
    session: &mut ImapSession,
    folder: &str,
    latest_datetime: &DateTime<FixedOffset>,
) -> HashSet<imap::types::Uid> {
    let search_filter = format!("NOT SEEN SINCE {}", latest_datetime.format("%d-%b-%Y"));
    session
        .select(folder)
        .expect("Cannot select specified folder");
    session.uid_search(search_filter).expect("Search failure")
}

struct EmailInfo {
    internal_date: DateTime<FixedOffset>,
    date: Option<String>,
    from: Option<String>,
    to: Option<String>,
    subject: Option<String>,
}

fn fetch_latest_messages(
    session: &mut ImapSession,
    uids: HashSet<imap::types::Uid>,
    latest_datetime: &DateTime<FixedOffset>,
) -> Vec<EmailInfo> {
    if uids.is_empty() {
        return Vec::new();
    }

    let mut uid_str = uids
        .iter()
        .fold(String::new(), |acc, uid| acc + &uid.to_string() + ",");

    // remove latest comma
    uid_str.pop();

    let fetch_result = session
        .uid_fetch(uid_str, "(UID FLAGS INTERNALDATE BODY.PEEK[HEADER])")
        .expect("Fetch messages failure");

    fetch_result
        .iter()
        .filter(|msg| {
            let internal_date =
                DateTime::parse_from_str(msg.internal_date().unwrap(), "%d-%b-%Y %H:%M:%S %z")
                    .expect("Message internal_date is not parsable");
            internal_date > *latest_datetime
        })
        .map(|msg| {
            let internal_date =
                DateTime::parse_from_str(msg.internal_date().unwrap(), "%d-%b-%Y %H:%M:%S %z")
                    .expect("Message internal_date is not parsable");
            let (headers, _) = parse_headers(
                msg.header()
                    .expect("Header are not present in fetch result"),
            )
            .expect("Unable to parse headers");
            let parse_error = String::from("Parse error");
            EmailInfo {
                internal_date,
                date: headers
                    .get_first_value("Date")
                    .unwrap_or(Some(parse_error.clone())),
                from: headers
                    .get_first_value("From")
                    .unwrap_or(Some(parse_error.clone())),
                to: headers
                    .get_first_value("To")
                    .unwrap_or(Some(parse_error.clone())),
                subject: headers
                    .get_first_value("Subject")
                    .unwrap_or(Some(parse_error.clone())),
            }
        })
        .collect()
}

fn build_notification_message(email: &EmailInfo, folder: &str) -> String {
    let mut msg = String::from("New message in ") + folder + "\n";

    if let Some(date) = &email.date {
        msg += &format!("Date: {}\n", date);
    } else {
        msg += "No date in message\n";
    }

    if let Some(from) = &email.from {
        msg += &format!("From: {}\n", from);
    } else {
        msg += "No from in message\n";
    }

    if let Some(to) = &email.to {
        msg += &format!("To: {}\n", to);
    } else {
        msg += "No to in message\n";
    }

    if let Some(subject) = &email.subject {
        msg += &format!("Subject: {}", subject);
    } else {
        msg += "No subject in message";
    }

    msg
}

fn get_xmpp_client(config: &XmppConfig) -> tokio_xmpp::Client {
    Client::new(&config.jid, &config.password).expect("Failed to connect to XMPP")
}

fn main() {
    let config = get_config(FilePath::DefaultUser);
    let mut latest = get_latest_information(FilePath::DefaultUser, &config.imap.folders);

    let mut notifications: Vec<String> = Vec::new();
    let mut imap_session = get_imap_session(&config.imap);

    println!("Get messages from IMAP Server {}", config.imap.domain);
    for folder in &config.imap.folders {
        let original_latest_datetime = if latest.datetime_by_folder.contains_key(folder) {
            latest.datetime_by_folder[folder]
        } else {
            let local_today = Local::today().and_hms(0, 0, 0);
            DateTime::<FixedOffset>::from_utc(local_today.naive_utc(), *local_today.offset())
        };
        let mut current_latest_datetime = original_latest_datetime;
        println!("Folder {}", folder);
        println!(
            "Latest date & time: {}",
            current_latest_datetime.format("%d-%b-%Y %H:%M:%S %z")
        );

        let uids = get_latest_unread_uid(&mut imap_session, folder, &original_latest_datetime);

        if !uids.is_empty() {
            println!("Fetch {} UIDs", uids.len());
            let mut xmpp_messages_to_send =
                fetch_latest_messages(&mut imap_session, uids, &original_latest_datetime)
                    .iter()
                    .map(|msg| {
                        // This should be moved somewhere else
                        if msg.internal_date > original_latest_datetime {
                            current_latest_datetime = msg.internal_date;
                        }

                        build_notification_message(&msg, folder)
                    })
                    .collect::<Vec<String>>();

            println!("Add {} notifications", xmpp_messages_to_send.len());
            notifications.append(&mut xmpp_messages_to_send);

            println!(
                "Update latest date & time to {}",
                current_latest_datetime.format("%d-%b-%Y %H:%M:%S %z")
            );
            latest
                .datetime_by_folder
                .insert(folder.to_owned(), current_latest_datetime);
        } else {
            println!("No UIDs to check");
        }

        println!("--------------------");
    }

    let mut rt = Runtime::new().expect("Tokio Runtime creation failure");
    let xmpp_client = get_xmpp_client(&config.xmpp);

    let (sink, stream) = xmpp_client.split();
    let mut sink_state = Some(sink);

    if notifications.is_empty() {
        println!("No notification to send");
    } else {
        println!(
            "Send {} notifications to {}",
            notifications.len(),
            config.xmpp.recipient_jid
        );
        let notifications = &notifications;
        let recipient_jid = config
            .xmpp
            .recipient_jid
            .parse::<Jid>()
            .expect("Cannot parce XMPP Recipient as JID");
        let done = stream.for_each(move |event| {
            if event.is_online() {
                let mut sink = sink_state.take().expect("Unknown error");
                let (last_n, notifications) = notifications.split_last().unwrap();
                for n in notifications {
                    let mut message = Message::new(Some(recipient_jid.clone()));
                    let content = n.to_owned() + "\n-----------------------";
                    message.bodies.insert(String::new(), Body(content));
                    sink.start_send(Packet::Stanza(message.into()))
                        .expect("Failure while sending XMPP notification");
                }

                // Send last notificaiton
                {
                    let mut message = Message::new(Some(recipient_jid.clone()));
                    message
                        .bodies
                        .insert(String::new(), Body(last_n.to_owned()));
                    sink.start_send(Packet::Stanza(message.into()))
                        .expect("Failure while sending XMPP notification");
                }

                sink.start_send(Packet::StreamEnd)
                    .expect("Failure while sending XMPP end of stream");
            }

            Box::new(future::ok(()))
        });

        match rt.block_on(done) {
            Ok(_) => (),
            Err(e) => eprintln!("Fatal error: {}", e),
        };
    }

    println!("Done sending notifications, save latest information");
    save_latest_information(FilePath::DefaultUser, latest);
}
